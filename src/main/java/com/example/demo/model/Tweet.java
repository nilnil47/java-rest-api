package com.example.demo.model;//package com.example.demo.model;

//import javax.persistence.Entity;

//@Entity
//@Table(name = "Tweets")
public class Tweet {

//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private  Long timestamp;
    private String username;
    private String content;

    public Tweet(String username, String content, Long timestamp) {
        this.username = username;
        this.content = content;
        this.timestamp = timestamp;
    }

    public Tweet() {

    }

    public String getId() {
        return username;
    }

    public String getContent() {
        return content;
    }
}
