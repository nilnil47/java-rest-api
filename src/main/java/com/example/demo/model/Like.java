package com.example.demo.model;

//@Entity
public class Like {

    private String username;
    private Long postId;
    private Long timestamp;

    public Like(String username, Long postId, Long timestamp) {
        this.username = username;
        this.postId = postId;
        this.timestamp = timestamp;
    }

    private Long id;

    public Like() {

    }

    public void setId(Long id) {
        this.id = id;
    }

//    @Id
    public Long getId() {
        return id;
    }
}
